# Модуль db (базовые методы) - документация разработчика на русском языке (Devices Shop)
	Описание: модуль db является надстройкой над сторонним модулем mongoose (mongoosejs.com)
	и содержит набор методов для управления базой данных Mongo с учётом связей между сущностями в БД.
Модуль db зависит от модулей underscore(в будущем поменяй на lodash) и mongoose.

! Отношения между сущностями описаны в ER-модели - см. файл "entity-relationships.jpg"

## Пункты
	- Список методов
	- Описание каждого метода:
		- Название
		- Принимаемые параметры
		- Примеры(ы) использования

## Список методов:
	- getInstances()
	- getManyToMany()

	- createInstances() - работает только для одного, но не для множества
	- createManyToMany() - не реализован

	- updateInstances() - работает только для одного, но не для множества
	- updateManyToMany() - не реализован

	- deleteInstances() - работает только для одного, но не для множества
	- deleteManyToMany() - не реализован

! Примечание: методы множественного выбора такие как getInstances(), createInstances(), updateInstances(), deleteInstances() <br>
могут использоваться для проведения операции при связи многие к одному, если передать дополнительный параметр query(это объект), в котором указать по какому полю выбирать и значение этого поля.
(http://docs/#getInstances)[См. пример]

! Также методы getInstances(), createInstances(), updateInstances(), deleteInstances() <br>
могут использоваться для проведения операции над одним экземпляром сущности, если передать параметр query(это объект) с полем _id со значением идентификатора этого экземпляра.

! Для проведения операций при связи многие ко многим существуют отдельные методы
getManyToMany(), createManyToMany(), updateManyToMany(), deleteManyToMany()



## getInstances()
### Параметры метода getInstances()
	! параметры передаются в виде объекта params
	- Entity - сущность, данные которой нужно получить;
		в js коде это ссылка на объект;
		этот объект является mongoose-моделью и содержит
		методы mongoose'а для манипуляции над сущностью в MongoDB;
	- callback - анонимная функция;
		внутри этой функции можно воспользоваться резельтатом операции, 
		например, отослать клиенту полученные из БД данные;
	- query - анонимный объект; в нём можно указать по какому полю 
		совершать выборку и значение этого поля.

1) Выборка одного элемента сущности. <br>
Для этого нужно передать параметр query(это объект) c полем _id со значением идентификатора этого экземпляра.
```javascript
	/* ! Для начала создайте mongoose-модель Product */
	let product_id = '73223fbd539c341f70cd05a3';
	let params = {
		Entity: Product,
		query: {_id: product_id},
		callback: (error, docs) => {
			response.send(docs);
		}
	}
	db.getInstances(params);
```

2) Выборка всех элементов сущности.
```javascript
	/* ! Для начала создайте mongoose-модель Product */
	let params = {
		Entity: Product,
		callback: (error, docs) => {
			response.send(docs);
		}
	}
	db.getInstances(params);
```

3) Выборка подмножества элементов. <br>
В этом случае нужно передать параметр query(объект), где указать по какому полю выбирать и с каким значением.
Например, выбрать все продукты, у которых цена равна 42.
```javascript
	/* ! Для начала создайте mongoose-модель Product */
	let params = {
		Entity: Product,
		query: {cost: 42},
		callback: (error, docs) => {
			response.send(docs);
		}
	}
	db.getInstances(params);
```

4) Выборка элементов при отношении многие к одному. <br>
Например, получить все продукты которые относятся к определённому ярлыку. <br>
У продукта один ярлык, но один ярлык может относиться ко многим продуктам. <br>
Для этого нужно передать id ярлыка, на который ссылается множество продуктов.
```javascript
	/* ! Для начала создайте mongoose-модели Product и Label */
	let label_id = '58123fbd539c341f057043cd';
	let params = {
		Entity: Product,
		query: {label_id: label_id},
		callback: (docs) => {
			response.send(docs);
		}
	}
	db.getInstances(params);
```

## getManyToMany()
### Как работает метод
1. получить все экземпляры смежной сущности <br>
2. получить массив айдишников из sumEntity, которые относятся к instanceId <br>
3. на основе этого массива получить нужные экземпляры сущности targetEntity <br>

### Параметры метода getManyToMany()
	! параметры передаются в виде объекта params
	- targetEntity - сущность, данные которой нужно получить;
		в js коде это ссылка на объект; этот объект является 
		mongoose-моделью и содержит методы mongoose'а для манипуляции над MongoDB;
	- instanceId - id экземпляра, на который ссылаются экземпляры целевой сущности;
	- sumEntity - суммарная сущность, которая хранит id экземпляров сущностей, 
		которые вступают в отношение многие ко многим;
	- callback - анонимная функция; внутри этой функции можно воспользоваться 
		резельтатом операции, например, отослать клиенту полученные из БД данные;

### Примеры getManyToMany()
1) Получить все теги, которые относятся к определённому продукту (много тегов ко многим продуктам) <br>
```javascript
	/* ! Для начала создайте mongoose-модели Tag и Products_Tags */
	let product_id = '59123fbd539c341f057043ce';
	let params = {
		targetEntity: Tag,
		instanceId: product_id,
		sumEntity: Products_Tags,
		callback: (docs) => {
			response.send(docs);
		}
	}
	db.getManyToMany(params);
```



## createInstances()
## createManyToMany()



## updateInstances()
## updateManyToMany()



## deleteInstances()
## deleteManyToMany()