'use strict';

const express = require('express');
const bodyParser = require('body-parser');

const config = require('./config');

const Product = require('./db/models/product/product');
const Label = require('./db/models/product/label');
const AvailableStatus = require('./db/models/product/availableStatus');
const Tag = require('./db/models/product/tag');
const Products_Tags = require('./db/models/product/products_tags');

const Order = require('./db/models/order');
const User = require('./db/models/user');
const Language = require('./db/models/language');

const Db = require('./db/db');
const db = new Db();

const app = express();

app.listen(config.server.port, () => {
	console.log('server listening at http://' + config.server.address + ':' + config.server.port + '/#/products');
});

/*******
* Routes *
********/

/** others **/
	app.use(express.static(__dirname + "/public"));
	app.use(bodyParser.json());

/** Products **/
	/* get */
	app.get('/product/:product_id', (request, response) => {
		let product_id = request.params.product_id;
		let params = {
			Entity: Product,
			query: {_id: product_id},
			callback: (error, docs) => {
				response.send(docs[0]);
			}
		}
		db.createInstances(params);
	});

	app.get('/products', (request, response) => {
		let params = {
			Entity: Product,
			callback: (error, docs) => {
				response.send(docs);
			}
		}
		db.getInstances(params);
	});

	app.get('/products/:order_id', (request, response) => {
		let order_id = request.params.order_id;
	});

	app.get('/products/:label_id', (request, response) => {
		let label_id = request.params.label_id;
		let params = {
			Entity: Product,
			query: {label_id: label_id},
			callback: (docs) => {
				response.send(docs);
			}
		}
		db.getInstances(params);
	});

	app.get('/products/:status_id', (request, response) => {
		let status_id = request.params.status_id;
	});

	app.get('/products/:tag_id', (request, response) => {
		let tag_id = request.params.tag_id;
	});

	/* put */
	app.put('/product/:product_id', (request, response) => {
		let product_id = request.params.objId;
		let data = request.body;
		db.updateInstances(Product, objId, data, (error, doc) => {
			response.send(objId);
		});
	});

	/* post */
	app.post('/product', (request, response) => {
		let data = request.body;
		db.createInstances(Product, data, (error, doc) => {
			response.send(doc._id);
		});
	});

	/* delete */
	app.delete('/product/:product_id', (request, response) => {
		let product_id = request.params.product_id;
		db.deleteInstances(Product, product_id, (error, doc) => {
			response.send(product_id);
		});
	});

/** Orders **/

/** Users **/

/** Labels of Products **/

/** Available Statuses of Products **/

/** Tags of Products **/
	app.get('/tags', (request, response) => {
		db.getInstances(Tag, (error, docs) => {
			response.send(docs);
		});
	});

	app.get('/tags/:product_id', (request, response) => {
		let product_id = request.params.product_id;
		let params = {
			targetEntity: Tag,
			instanceId: product_id,
			sumEntity: Products_Tags,
			callback: (docs) => {
				response.send(docs);
			}
		}
		db.getManyToMany(params);
	});

/* products_tags */
	app.post('/products_tags', (request, response) => {
		let data = request.body;
		db.createInstances(Products_Tags, data, (error, doc) => {
			response.send(doc);
		});
	});

/* labels */
	app.get('/productLabels', (request, response) => {
		db.getInstances(ProductLabel, (error, docs) => {
			response.send(docs);
		});
	});

	app.post('/productLabels', (request, response) => {
		let data = request.body;
		db.createInstances(ProductLabel, data, (error, doc) => {
			response.send(doc._id);
		});
	});

	app.put('/productLabels/:objId', (request, response) => {
		let objId = request.params.objId;
		let data = request.body;
		db.updateInstances(ProductLabel, objId, data, (error, doc) => {
			response.send(objId);
		});
	});

	app.delete('/productLabels/:objId', (request, response) => {
		let objId = request.params.objId;
		db.deleteInstances(ProductLabel, objId, (error, doc) => {
			response.send(objId);
		});
	});

/* availableStatuses */
	app.post('/availableStatus', (request, response) => {
		let data = request.body;
		let params = {
			Entity: AvailableStatus,
			data: data,
			callback: (error, doc) => {
				response.send(doc._id);
			}
		}
		db.createInstances(params);
	});