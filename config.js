'use strict';

module.exports = {
	server: {
		port: 8080,
		address: 'localhost'
	},
	db: {
		address: 'localhost',
		port: 27017,
		name: 'devices'
	}
}