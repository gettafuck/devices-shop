'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Products_OrdersSchema = new Schema({
	product_id: String,
	order_id: String
});

const Products_Orders = mongoose.model('Products_Orders', Products_OrdersSchema);

module.exports = Products_Orders;