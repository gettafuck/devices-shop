'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Products_TagsSchema = new Schema({
	product_id: String,
	tag_id: String
});

const Products_Tags = mongoose.model('Products_Tags', Products_TagsSchema);

module.exports = Products_Tags;