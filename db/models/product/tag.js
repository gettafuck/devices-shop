'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TagSchema = new Schema({
	text: String,
	priority: Number
});

const Tag = mongoose.model('Tag', TagSchema);

module.exports = Tag;