'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const LabelSchema = new Schema({
	text: String,
	color: String
});

const Label = mongoose.model('Label', LabelSchema);

module.exports = Label;