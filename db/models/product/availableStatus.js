'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const AvailableStatusSchema = new Schema({
	text: String,
	color: String
});

const AvailableStatus = mongoose.model('AvailableStatus', AvailableStatusSchema);

module.exports = AvailableStatus;