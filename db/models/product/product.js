'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ProductSchema = new Schema({
	name: String,
	cost: Number,
	oldCost: Number,
	description: String,
	imgUrl: String,
	label_id: String,
	availableStatus_id: String,
	language_id: String
});

const Product = mongoose.model('Product', ProductSchema);

module.exports = Product;