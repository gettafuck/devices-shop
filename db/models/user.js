'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
	name: String,
	contacts: {
		phone: String,
		email: String
	},
	role: String,
	login: String,
	password: String,
	imgUrl: String
});

const User = mongoose.model('User', UserSchema);

module.exports = User;