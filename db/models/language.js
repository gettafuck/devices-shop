'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const LanguageSchema = new Schema({
	name: String,
	code: String
});

const Language = mongoose.model('Language', LanguageSchema);

module.exports = Language;