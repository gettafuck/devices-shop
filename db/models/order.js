'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const OrderSchema = new Schema({
	user_id: String,
	totalCost: Number
});

const Order = mongoose.model('Order', OrderSchema);

module.exports = Order;